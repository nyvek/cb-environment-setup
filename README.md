Author: Kevyn Quiros
Last Update: Oct 18, 2017

Dependencies
* VirtualBox (brew cask)
* Docker
* Minikube (brew cask)

Minimum required repos
* originations
* cb-base-application
* cb-auth-application
* cb-base-loan-application
* js-core
* css-core

Clone and build every front end repo you need as commanded in the repo�s readme

Steps
1. Start kubernetes
    * `minikube start`
2. Inside originations directory run the kubernetes shell script
    * `sh refi_minikube.sh`
3. Go to the kubernetes directory in originations
    * `cd cb-api/minikube`
4. In the file named api-deployment.yml we�ll need to replace line 22 with this:
    * `value: "-Dhost.cookie_domain=.commonbond.dev -Dredis.host=redis -Dmsapi_base_url=http://msapi:8080/cb-msapi/api -Dtermination_base_url=http://cb-api:8080/originations/api/loans -Dswagger.host=cb-api -Demailapi_base_url=http://emailapi:8080/cb-emailapi/api -Dms_endpoint=https://modelshop-test.commonbond.co:8443/modelshop -Delasticsearch.active=false -Dcors.supported_domains=local.commonbond.dev,local-gud.commonbond.dev,local-refi.commonbond.dev,local-refi-cosign.commonbond.dev,local-auth.commonbond.dev,local-products.commonbond.dev,local-school.commonbond.dev,local-personal.commonbond.dev"`
5. Still inside that directory run this
    * `kubectl apply -f api-deployment.yml`
6. We'll need to figure out the port in which originations is running, so back in the root of originations run
    * `kubectl describe services/cb-api -n refi-minikube`
7. The port is the one under NodePort (Ex: https  30757/TCP), you want to grab the number that's jsut before /TCP, in my case 30757
8. Now, the pod's IP is a bit different to obtain, it is not the one displayed in the logs above. So the IP usually is 192.168.99.100 but to be sure you can run the following command and it will take you to the Kubernetes Dashboard, there, in the address you'll see which IP it is right before the port.
9. Write down your IP:Port, we'll need it later, mine looks like this `192.168.99.100:30757`
10. To make sure originations is running go to this address:
    * http://[IP]:[Port]/originations/api/users/me (Ex: http://192.168.99.100:30757/originations/api/users/me)
11. Now, for the front-end part we're going to need to set the hosts a bit different, so your host file, which should be under `/etc/hosts` should have this:
    ```
    # CB Hosts
    127.0.0.1   localhost
    192.168.99.100   local.commonbond.dev
    127.0.0.1   local-auth.commonbond.dev
    127.0.0.1   local-student.commonbond.dev
    127.0.0.1   local-student-cosign.commonbond.dev
    127.0.0.1   local-legacy.commonbond.dev
    127.0.0.1   local-gud.commonbond.dev
    127.0.0.1   local-refi.commonbond.dev
    127.0.0.1   local-refi-cosign.commonbond.dev
    127.0.0.1   local-products.commonbond.dev
    127.0.0.1   local-home.commonbond.dev
    127.0.0.1   brand.commonbond.dev
    127.0.0.1   local.apache.dev
    192.168.99.100   local-kube.commonbond.dev 
    ```
12. Notice that *local-kube.commonbond.dev* and *local.commonbond.dev*, in this case, if your IP happens to be different you'll need to update it to yours
13. Once you do that you'll need to go to cb-base-app, find a file name local.js under src/js/config/environment and go to the property apiUrl. You'll need to replace it's value for it to look something like this 
    *http://local-kube.commonbond.dev:[Port]/originations/api/
14. Once you're done with all of that you'll need to rebuild all the FE projects with webpack starting with cb-base-application
